package OpdrachtAutoMetAanhang;

public class Auto {
    private String type ;
    private String merk ;
    private int pk ;
    private boolean opgevoerd;
    private Aanhanger aanhang;
    //private String autos = "Tesla S 80, Skoda Fabia 50, Ferrari F40 400";

    //<editor-fold desc="Constructors">
    public Auto(String merk, String type, int pk, boolean metAanhanger) {
        this.merk = merk;
        this.type = type;
        this.pk = pk;
        if (metAanhanger) {
            aanhang = new Aanhanger();
        }
    }

    public Auto(boolean heeftAanhanger, String merk) {
        this.merk = merk;
        this.type = "";
        this.pk = 30;

        if (merk.equals("Tesla")) {
            type = "S";
            pk = 80;
        }
        if (merk.equals(("Skoda"))){
            type = "Fabia";
            pk = 40;
        }
        if (merk.equals("Ferari")) {
            type = "f40";
            pk = 400;
        }

        if (heeftAanhanger) {
            aanhang = new Aanhanger();
        }
    }
    //</editor-fold>

    public Aanhanger getAanhang() {
        return aanhang;
    }

    public boolean heeftAanhanger() {
        return this.aanhang != null;
    }

    //Afkoppelen, dus ook de aanhanger hangt niet meer aan een auto
    public void koppelAf() {
        if (heeftAanhanger()) {
            if (aanhang.isGekoppeld()) {
                aanhang.koppelAf();
            }
            aanhang = null;
        }
    }

    //Alleen aankoppelen als er nog niets aanhangt.
    public void setAanhanger(Aanhanger eraan) {
        if (!heeftAanhanger() ) {
            aanhang = eraan;
        }
    }

    @Override
    public String toString() {
        return "Auto{" +
                "type='" + type + '\'' +
                ", merk='" + merk + '\'' +
                ", pk=" + pk +
                ", opgevoerd=" + opgevoerd +
                ", aanhang=" + aanhang +
                '}';
    }
}

