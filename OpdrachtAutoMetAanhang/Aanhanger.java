package OpdrachtAutoMetAanhang;

public class Aanhanger {

    //<editor-fold desc="Privates">
    private int aantalWielen;
    private int lengte;
    private Auto aanDeze;
    //</editor-fold>

    //<editor-fold desc="Constructors">
    public Aanhanger() {
        this(2);
    }

    boolean isGekoppeld () {
        return  aanDeze != null;
    }

    public Aanhanger(int aantalWielen) {
        this(aantalWielen,aantalWielen * 2);
    }


    public Aanhanger(int aantalWielen, int lengte) {
        this.aantalWielen = aantalWielen;
        this.lengte = lengte;
    }
    //</editor-fold>

    //<editor-fold desc="AantalWielenEnLengte">
    int getAantalWielen() {
        return aantalWielen;
    }
    void setAantalWielen(int aantal) {
        if (!isGekoppeld()) {
            aantalWielen = aantal;
        }
    }

    int getLengte() {
        return lengte;
    }
    void setLengte(int meters) {
        if (!isGekoppeld()) {
            lengte = meters;
        }
    }
    //</editor-fold>

    @Override
    public String toString() {
        return "Aanhanger{" +
                "aantalWielen=" + aantalWielen +
                ", lengte=" + lengte +
                '}';
    }

    public void koppelAf() {
        if (this.isGekoppeld()){
            Auto tijdelijk = aanDeze;
            aanDeze = null;
            if (tijdelijk.heeftAanhanger()) {
                tijdelijk.koppelAf();
            }
        }
    }
}
