package OpdrachtAutoMetAanhang;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Program {
    static BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {
        System.out.println("Welk merk?");
        //iets over het lezen van input
        String merk = consoleReader.readLine();
        Auto mijnAuto = new Auto(true, merk);
        Aanhanger mijnAanhanger = mijnAuto.getAanhang();

        System.out.println(mijnAuto);
        mijnAuto.koppelAf();
        System.out.println("----- na afkoppelen ------");
        System.out.println(mijnAuto);
        System.out.println(mijnAanhanger);
    }
}
